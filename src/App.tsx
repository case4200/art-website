
//import './App.css'
import ListGroup from "./components/ListGroup";

function App() {
  // App.tsx

  let items = [
    'Paper',
    'Notebook',
    'Cardboard',
    'Cardstock',
    'Newspaper'
  ];


  return (

    <div>
      <ListGroup items={items} heading="Papers" />
    </div>
  );

}

export default App;
